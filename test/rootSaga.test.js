import expect from 'expect';
import { fork } from 'redux-saga/effects';
import startForeman from '../src/sagas/rootSaga';
import watchSearchMedia from '../src/sagas/watcher';


describe('Test startForeman saga', () => {
  it('should yield array watchers saga', () => {
    expect(startForeman().next().value).toEqual(fork(watchSearchMedia));
  });
});