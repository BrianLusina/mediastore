import React from 'react';
import { render } from 'react-dom';
import Header from '../src/common/Header';
import expect from 'expect';
import { shallow } from 'enzyme';
import { Link, IndexLink } from 'react-router';


describe("Header common component", function(){
    // this ensures that we test for the header component without affecting other elements 
    // indirectly
    var wrapper;
    beforeEach(() => {
        wrapper = shallow(<Header />);
    });
    
    // check that the component renders without a crash
    it('renders without crashing', () => {
        const div = document.createElement('div');
        render(<wrapper />, div);
    });

    // test to check that the library component link is there
    it("Has links to library component", function(){
        expect(wrapper.find(Link).length).toEqual(1);
    });

    // check if the index link is available
    it("Has an index link", function(){
        expect(wrapper.find(IndexLink).length).toEqual(1);
    });
})

