import React from 'react';
import { render } from 'react-dom';
import HomePage from '../src/components/HomePage';
import expect from 'expect';
import { shallow } from 'enzyme';
import { Link } from 'react-router';


describe("HomePage component", function(){
    // this ensures that we test for the component without affecting other elements 
    // indirectly
    var wrapper;
    beforeEach(() => {
        wrapper = shallow(<HomePage />);
    });
    
    // check that the component renders without a crash
    it('renders without crashing', () => {
        const div = document.createElement('div');
        render(<wrapper />, div);
    });

    // test to check that the library component link is there
    it("has a link", function(){
        expect(wrapper.find(Link).length).toEqual(1);
    });

    it("has a link is to the library component", function(){
        expect(wrapper.find(Link).props().to).toEqual("library");
    });
})

