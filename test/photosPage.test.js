import expect from 'expect';
import React from 'react';
import { mount } from 'enzyme';
import PhotosPage from '../src/components/PhotosPage';

describe("PhotosPage component tests", () => {
    const setUp = () => {
        const props = {
            images :[{id: 1, test:"test image"}],
            onHandleSelectImage: expect.createSpy(),
            selectedImage:{id:1, test:"test image"}
        }
        const Wrapper = mount(<PhotosPage {...props} />);
        return { Wrapper };        
    };

    const { Wrapper } = setUp();

    it("Should assert that component exists", function(){
        expect(Wrapper).toExist();
    });

    it("should have render props", function(){
        expect(Wrapper.props().images).toEqual([{id:1, test:"test image"}]);
        expect(typeof Wrapper.props().onHandleSelectImage).toEqual("function");
        expect(Wrapper.props().selectedImage).toEqual({id:1, test:"test image"});
    });

    //test.todo('fix hasClass issue');
    it("should render self", function(){
        expect(Wrapper.find("h2").text()).toEqual("Images");
        //expect(Wrapper.find("h6").hasClass("title")).toBe("true");
        expect(Wrapper.find("img").length).toEqual(2);
        expect(Wrapper.find("div").length).toEqual(5);
    });

});
    