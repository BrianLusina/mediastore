import React from 'react';
import expect from 'expect';
import { shallow, mount } from 'enzyme';
import { MediaGalleryPage } from '../src/containers/MediaGalleryPage';

const setup = () => {
    const props = {
        _handleSearch: expect.createSpy(),
        _handleSelectImage: expect.createSpy(),
        _handleSelectVideo:expect.createSpy(),
        dispatch: expect.createSpy(),
        ref: expect.createSpy(),
        value:"ref",
        images: [{id:1, mediaUrl:"testImageUrl"}],
        videos: [{id:1, mediaUrl:"testVideoUrl"}],
        selectedVideo: {id:1, mediaUrl:"testVideoUrl"},
        selectedImage: {id:1, mediaUrl:"testImageUrl"}
    };

    const Wrapper = shallow(<MediaGalleryPage {...props}/>);
    return {Wrapper, props};
}

describe("MediaGalleryPage tests", function(){

    it('should render self and subcomponents', () => {
        const { Wrapper } = setup();
        
        expect(Wrapper.find('div').length).toEqual(3);
        const PhotosPageWrapper = Wrapper.find('PhotosPage').props();
        const VideosPageWrapper = Wrapper.find('VideosPage').props();
        expect(PhotosPageWrapper.images).toEqual([{ id: 1, mediaUrl: 'testImageUrl' }]);
        expect(PhotosPageWrapper.selectedImage).toEqual({ id: 1, mediaUrl: 'testImageUrl' });
        expect(typeof PhotosPageWrapper.onHandleSelectImage).toBe('function');
        expect(VideosPageWrapper.videos).toEqual([{ id: 1, mediaUrl: 'testVideoUrl' }]);
        expect(VideosPageWrapper.selectedVideo).toEqual({ id: 1, mediaUrl: 'testVideoUrl' });
        expect(typeof VideosPageWrapper.onHandleSelectVideo).toBe('function');
    });

    it('should call dispatch on onHandleSelectImage', () => {
        const { Wrapper, props } = setup();

        const input = Wrapper.find('PhotosPage');
        input.props().onHandleSelectImage({ id: 1, mediaUrl: 'testImageUrl' });
        expect(props.dispatch.calls.length).toBe(1);
    });

    it('should call dispatch onHandleSelectVideo', () => {
        const { Wrapper, props } = setup();

        const input = Wrapper.find('VideosPage');
        input.props().onHandleSelectVideo({ id: 1, mediaUrl: 'testVideoUrl' });
        expect(props.dispatch.calls.length).toBe(1);
    });
    
    it('should call dispatch on onClick event', () => {
        const { props } = setup();
        const testWrapper = mount(<MediaGalleryPage {...props} />)
        const input = testWrapper.find('input').last();
        const event = { preventDefault: () => {} };
        input.simulate('click', event);
        expect(props.dispatch.calls.length).toBe(2);
    });
});