import nock from 'nock';
import 'isomorphic-fetch';
import expect from 'expect';
import { flickrImages, shutterStockVideos } from '../src/api/api';

describe("Api tests", function(){
    it("should call flickr api", function(){
        flickrImages("rain")
            .then((res) => {
                expect(res.length).toEqual(10);
            });
    });

    it("should call shutterstock api", function(){
        shutterStockVideos("sun")
            .then((res)=>{
                expect(res.length).toEqual(10);
            });
    });

});