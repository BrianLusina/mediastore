import expect from 'expect';
import { take, fork } from 'redux-saga/effects';
import searchMediaSaga from '../src/sagas/mediaSaga';
import watchSearchMedia from '../src/sagas/watcher';

describe("Test for watch load Flickr images", function(){
    describe("Test for watch search media", function(){
        it("should call searchMediaSaga", function(){
            const gen = watchSearchMedia();
            const action = {type: "SEARCH_MEDIA_REQUEST"};
            expect(gen.next().value).toEqual(take("SEARCH_MEDIA_REQUEST"));
            expect(gen.next(action).value).toEqual(fork(searchMediaSaga, action));
        });
    });
});
