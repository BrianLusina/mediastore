import React from 'react';
import { render } from 'react-dom';
import App from '../src/containers/App';
import { shallow } from 'enzyme';
import expect from 'expect';

describe("App Container", function(){
  var wrapper;

  beforeEach(() => {
    const props = ["test1", "test2"];
    wrapper = shallow(<App children={props} />);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    render(<wrapper />, div);
  });

  it("has a header component", ()=>{
    expect(wrapper.find("Header").length).toEqual(1);
  });

  it("has a container with class App-header", function(){
    expect(wrapper.find("div").hasClass("App-header")).toBe(true);
  });
  
  it("renders children", function(){
    expect(typeof wrapper.props().children).toBe("object");
  });
});
