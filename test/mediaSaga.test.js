import expect from 'expect';
import { put, call } from 'redux-saga/effects';
import searchMediaSaga from '../src/sagas/mediaSaga';
import { flickrImages, shutterStockVideos } from '../src/api/api';

describe("Media Saga tests", ()=>{
    const payload = "test";
    const gen = searchMediaSaga({payload});

    it("Should call Shutterstock videos API", function(){
        expect(gen.next(payload).value).toEqual(call(shutterStockVideos, payload));
    });

    it("should call Flickr Images API", ()=>{
        expect(gen.next(payload).value).toEqual(call(flickrImages, payload));
    });

    it("should yield array of objects", ()=>{
        const videos = [];
        expect(gen.next(videos).value.length).toEqual(4);
    });

    it("should dispatch failure effect", ()=>{
        const error = "error";
        expect(gen.throw(error).value).toEqual(put({type:"SEARCH_MEDIA_ERROR", error}));
    });
})