# Media Store/Library

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d1842c7420004a49889c0412aa81f9d1)](https://www.codacy.com/app/BrianLusina/mediastore?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=BrianLusina/mediastore&amp;utm_campaign=Badge_Grade)
[![Build Status](https://travis-ci.org/BrianLusina/mediastore.svg?branch=master)](https://travis-ci.org/BrianLusina/mediastore)
[![codecov](https://codecov.io/gh/BrianLusina/mediastore/branch/master/graph/badge.svg)](https://codecov.io/gh/BrianLusina/mediastore)
[![CircleCI](https://circleci.com/gh/BrianLusina/mediastore.svg?style=svg)](https://circleci.com/gh/BrianLusina/mediastore)
[![Dependency Status](https://dependencyci.com/github/BrianLusina/mediastore/badge)](https://dependencyci.com/github/BrianLusina/mediastore)
[![Dependency Status](https://gemnasium.com/badges/github.com/BrianLusina/mediastore.svg)](https://gemnasium.com/github.com/BrianLusina/mediastore)

A media library application built on React and Redux

Simply clone the project and run

``` sh
$ npm install
$ npm run start
```
> This should start a server and enable live reload


## Tests

Run tests with

``` sh
$ npm run test
```

Coverage reports are generated with `jest`.