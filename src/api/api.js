import { config } from 'dotenv';

// const FLICKR_API_KEY = process.env.FLICKR_API_KEY;
// const SHUTTERSTOCK_CLIENT_ID = process.env.SHUTTERSTOCK_CLIENT_ID;
// const SHUTTERSTOCK_SECRET = process.env.SHUTTERSTOCK_SECRET;

const FLICKR_API_KEY="a46a979f39c49975dbdd23b378e6d3d5";
const SHUTTERSTOCK_CLIENT_ID="3434a56d8702085b9226";
const SHUTTERSTOCK_SECRET="7698001661a2b347c2017dfd50aebb2519eda578";


// basic authentication to fech Shutterstock authentication
const basicAuth = () => "Basic ".concat(window.btoa(`${SHUTTERSTOCK_CLIENT_ID}:${SHUTTERSTOCK_SECRET}`));

const authParams = {
    headers:{
        Authorization: basicAuth()
    }
};

/** 
 * Description: [Access Shutterstock search enpoint for short videos]
 * @param {String} searchQuery
 * @return {Array}
 * */
export const shutterStockVideos = (searchQuery) => {
    const SHUTTERSTOCK_ENDPOINT = `https://api.shutterstock.com/v2/videos/search?query=${searchQuery}&page=1&per_page=10`;
    return fetch(SHUTTERSTOCK_ENDPOINT, authParams)
        .then(response =>{
            return response.json();
        },(reason)=>{
            console.error(reason);
        }).then(json => {
            return json.data.map(({id, assets, description})=>({
                id, 
                mediaUrl: assets.preview_mp4.url,
                description
            }));
        }, function(reason){
            console.error(reason);
        });
}

/**
 * Access flickr search endpoint for photos
 * @param {String} searchQuery
 * @returns {Array}
 */

export const flickrImages = (searchQuery)=>{
    const FLICKR_API_ENDPOINT = `https://api.flickr.com/services/rest/?method=flickr.photos.search&text=${searchQuery}&api_key=${FLICKR_API_KEY}&format=json&nojsoncallback=1&per_page=10`;

    //test.todo('update rejection promise callback');
    return fetch(FLICKR_API_ENDPOINT)
        .then(response =>{
            return response.json();
        },(reason)=>{
            console.error(reason);
        })
        .then(json => {
            return json.photos.photo.map(({farm, server, id, secret, title}) => ({
                id,
                title,
                mediaUrl: `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`
            }));
        }, (reason)=>{
            console.error(reason);
        })
}