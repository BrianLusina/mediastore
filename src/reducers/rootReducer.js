import { combineReducers } from 'redux';
import images from './imageReducer';
import videos from './videoReducer';

/**
 * Combines all reducers to a single reducer function
 * This registers the reducers before passing it to the store
 */
const rootReducer = combineReducers({
    images, videos
});

export default rootReducer;
