/**
 * reducers take the current state tree and an action object and then evaluate and return the outcome.
 */
export default {
  images: [],
  videos: []
};