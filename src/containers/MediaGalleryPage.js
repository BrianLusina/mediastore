import React, {PropTypes, Component } from 'react';
//import { flickrImages, shutterStockVideos } from '../api/api';
import { connect } from 'react-redux';
import PhotosPage from '../components/PhotosPage';
import VideosPage from '../components/VideosPage';
import { selectImageAction, selectVideoAction, searchMediaAction } from '../actions/mediaActions';

/**
 * Media Gallery Component
 * Sync react components with the redux store
 * Pass props to presentational components PhotosPage and VideosPage
 */
export class MediaGalleryPage extends Component{
    constructor(){
        super();
        this._handleSelectedImage = this._handleSelectedImage.bind(this);
        this._handleSelectedVideo = this._handleSelectedVideo.bind(this);
        this._handleSearch = this._handleSearch.bind(this);
    }

    /**Get images from api right after the component renders 
     * Uses the dispatch method from the store to dispatch 'searchMediaAction' immediately
     * after mounting.
    */
    componentDidMount(){
        this.props.dispatch(searchMediaAction("rain"));
    }

    _handleSelectedImage(selectedImage){
        this.props.dispatch(selectImageAction(selectedImage));
    }

    _handleSelectedVideo(selectedVideo){
        this.props.dispatch(selectVideoAction(selectedVideo));
    }

    _handleSearch(event){
        event.preventDefault();
        if(this.query != null){
            this.props.dispatch(searchMediaAction(this.query.value));
            this.query.value = "";
        }
    }

    /**
     * Each of the custom functions dispatches an action to the store when called. 
     * We use ref to save a callback that would be executed each time a user wants to search the library.
     */
    render(){
        const { images, selectedImage, videos, selectedVideo } = this.props;
        return(
            <div>
                {images ? 
                    <div>
                        <input type="text" ref={ref=>(this.query = ref)}/>
                        <input type="submit" value="Search Library" onClick={this._handleSearch}/>
                        <div className="row">
                            <PhotosPage
                                images={images}
                                selectedImage={selectedImage}
                                onHandleSelectImage={this._handleSelectedImage}
                            />
                            <VideosPage
                                videos={videos}
                                selectedVideo={selectedVideo}
                                onHandleSelectVideo={this._handleSelectedVideo}
                            />                            
                        </div>
                    </div>
                    : "loading..."
                }
            </div>
        )
    }
}


MediaGalleryPage.propTypes = {
  images: PropTypes.array,
  selectedImage: PropTypes.object,
  videos: PropTypes.array,
  selectedVideo: PropTypes.object,
  dispatch: PropTypes.func.isRequired
};

/**
 * Subscribe component to redux store and merge state into components
 * allows us keep in sync with store's updates and to format our state values before 
 * passing as props to the React component. We use ES6 destructuring assignment to extract 
 * images and videos from the store’s state.
 * https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
 */
const mapStateToProps = ({images, videos}) => ({
    images :images[0],
    selectedImage:images.selectedImage,
    videos: videos[0],
    selectedVideo: videos.selectedVideo
});

/**
 * connect method from react router connects the component to redux store
 * this will return a function that will take MediaGalleryPage as an argument
 */
export default connect(mapStateToProps)(MediaGalleryPage);