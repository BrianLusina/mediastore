import React, { Component, PropTypes } from 'react';
import logo from '../svg/logo.svg';
import '../styles/App.css';
import Header from '../common/Header';

/*The parent component that renders the header and links the user navigates to
this.props.children will be all other components as this is the parent component*/
class App extends Component {
  
  render() {
    return (
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Header />
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children : PropTypes.object.isRequired
}

export default App;
