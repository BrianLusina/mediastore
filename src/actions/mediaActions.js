/**
 * Defines action creators for the different actions we need
 * Uses the action types file
 * The optional arguments in the action creators: 
 * payload, image, and video are passed at the site of call/dispatch. 
 * Say, a user selects a video clip on our app, 
 * selectVideoAction is dispatched which returns SELECTED_VIDEO action type and the selected video as payload. 
 * Similarly, when searchMediaAction is dispatched, SEARCH_MEDIA_REQUEST action type and payload are returned.
 */

import * as types from '../constants/actionTypes';

/**
 * returns an action type, SELECTED_IMAGE and the image selected
 */
export const selectImageAction = (image)=>({
    type: types.SELECTED_IMAGE,
    image
});

/**
 * returns an action type, SELECTED_VIDEO and the video selected
 */
export const selectVideoAction = (video)=>({
    type: types.SELECTED_VIDEO,
    video
});

/**
 * returns an action type, SEARCH_MEDIA_REQUEST and search media request as a payload
*/
export const searchMediaAction = (payload) => ({
    type: types.SEARCH_MEDIA_REQUEST,
    payload
})
