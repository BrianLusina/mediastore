import React from 'react';
import { Link } from 'react-router';

/**Stateless functional HomePage Component */
const HomePage = () => (
    <div>
        <h1>Welcome to MediaStore</h1>
        <div>
            <Link to="library">
                <button> Visit Library</button>
            </Link>
        </div>
    </div>
)

export default HomePage;