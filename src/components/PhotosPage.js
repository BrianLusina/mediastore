import React, { PropTypes } from 'react';

/**
 * PhotosPage component
 * First, we extract images, onHandleSelectImage, and selectedImage from 
 * props using ES6 destructuring assignment and then render.
 */
const PhotosPage = ({images, onHandleSelectImage, selectedImage}) =>(
    <div>
        <h2>Images</h2>
        <div className="selected-image">
            <div id={selectedImage.id}>
                <div>
                    <h6>{selectedImage.title}</h6>
                    <img src={selectedImage.mediaUrl} alt={selectedImage.title}/>
                </div>
            </div>
            <div className="image-thumbnail">
                {images.map((image, i) => {
                    return <div key={i} onClick={onHandleSelectImage.bind(this, image)}>
                        <img src={image.mediaUrl} alt={image.title}/>
                    </div>
                })}
            </div>
        </div>
    </div>
)

PhotosPage.propTypes = {
    images : PropTypes.array.isRequired,
    selecteImage: PropTypes.object,
    onHandleSelectImage: PropTypes.func.isRequired
}

export default PhotosPage;
