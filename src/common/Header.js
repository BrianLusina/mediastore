import React from 'react';
import {Link, IndexLink } from 'react-router';

/**Stateless Component responsible for presentation
 * This will be used to display a header, so the business logic will be handled by the containers.
*/
const Header = ()=> (
    <div className="center">
        <nav className="navbar">
            <IndexLink to="/" activeClassName="active">Home</IndexLink>
            {" | "}
            <Link to="library" activeClassName="active">Library</Link>
        </nav>
    </div>
)

export default Header;